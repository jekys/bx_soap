<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arIncludeModules = Array(
   'webservice', //Обязательный модуль для работы сервера SOAP
   //Здесь дописать все используемые модули 
);

foreach ($arIncludeModules as $moduleName) {
   if(!CModule::IncludeModule($moduleName))
   return;   
}
   
// наш новый класс наследуется от базового IWebService
class CSoapService extends IWebService
{
   // метод GetWebServiceDesc возвращает описание сервиса и его методов
   function GetWebServiceDesc() 
   {
      $wsdesc = new CWebServiceDesc();
      $wsdesc->wsname = "soap.webservice"; // название сервиса
      $wsdesc->wsclassname = "CSoapService"; // название класса
      $wsdesc->wsdlauto = true;
      $wsdesc->wsendpoint = CWebService::GetDefaultEndpoint();
      $wsdesc->wstargetns = CWebService::GetDefaultTargetNS();

      $wsdesc->classTypes = array();
      $wsdesc->structTypes = Array();
      $wsdesc->classes = array();

      return $wsdesc;
   }
}

$arParams["WEBSERVICE_NAME"] = "soap.webservice";
$arParams["WEBSERVICE_CLASS"] = "CSoapService";
$arParams["WEBSERVICE_MODULE"] = "";

// передаем в компонент описание веб-сервиса
$APPLICATION->IncludeComponent(
   "bitrix:webservice.server",
   "",
   $arParams
   );

die();
?>
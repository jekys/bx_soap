# Инициализация тестового веб-сервиса #

Создаем в корне проекта папку local и в ней папку components
```
#!bash

mkdir -p /home/bitrix/www/local/components


```

Клонируем репозиторий
```
#!bash

cd /home/bitrix/www/local/components/
git clone https://bitbucket.org/jekys/bx_soap.git
```

Размещаем вызов компонента веб-сервиса в публичной части
```
#!bash

mv /home/bitrix/www/local/components/bx_soap/soap/ /home/bitrix/www/soap/
```

Заходим на урл: http://{АДРЕС_САЙТА}/soap/ws.php и убеждаемся, что веб-сервис работает

# Описание структур данных #

Типы данных для описания структуры, поддерживаемые веб-сервисом Bitrix

* string
* integer|int 
* boolean|bool
* double
* float|number
* base64|base64Binary
* any

Пример описания структуры данных:
```
#!php

//Струкутра "Компания"
$wsdesc->structTypes['company'] = Array(
 'id' => Array('varType' => 'int'), //ID
 'name' => Array('varType' => 'string'), //Название компании
 'phone' => Array('varType' => 'string') //Телефон
);
```

Описание стурктуры, где некоторые элементы являеются необязательными при передаче
```
#!php

//Струкутра "Компания"
$wsdesc->structTypes['company'] = Array(
 'id' => Array('varType' => 'int'), //ID
 'name' => Array('varType' => 'string'), //Название компании
 'phone' => Array('varType' => 'string', 'strict' => 'no') //Телефон (необязательное поле)
);
```

Описание структуры, где в качестве элемента используется другая структура
```
#!php

//Струкутра "Контактные данные"
$wsdesc->structTypes['contacts'] = Array(
	'phone' => Array('varType' => 'string', 'strict' => 'no'), //Телефон (необязательное поле)
	'email' => Array('varType' => 'string', 'strict' => 'no'), //Электронная почта (необязательное поле)
	'site' => Array('varType' => 'string', 'strict' => 'no'), //Веб-сайт (необязательное поле)
);


//Струкутра "Компания"
$wsdesc->structTypes['company'] = Array(
 	'id' => Array('varType' => 'int'), //ID
 	'name' => Array('varType' => 'string'), //Название компании
	'contacts' => Array('varType' => 'contacts'), //Контакты компании (структура описанна выше)
);
```

Пример описания структуры, где в качестве одного из элементов передается массив из элементов любого типа: